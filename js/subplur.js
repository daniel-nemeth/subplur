/**
 * Class dnSubPlur
 *
 * Substitute with pluralize
 *
 * @copyright   Copyright (C) 2018 Daniel Nemeth
 * @author      Daniel Nemeth <office@daniel-nemeth.com>
 */
var dnSubPlur = new jqClass({

    /**
     * Substitute [text] within an string
     */
    substitute: function(strReplace, objReplace)
    {
        return strReplace.replace(/\[(.+?)\]/g, function($0, $1) {
            return $1 in objReplace ? objReplace[$1] : $0;
        });
    },


    /**
     * Pluralize an string
     * [num]: Number of occurences within a string
     * {are|is} for whole words
     * {s} for adding an letter at the end
     */
    pluralize: function(strPluralize, intNum)
    {
        var indx = intNum == 1 ? 1 : 0;

        strPluralize = strPluralize.replace(/\[num\]/g, intNum);
        strPluralize = strPluralize.replace(/{(.[^}]*)}/g, function(wholematch, firstmatch) {
            var values = firstmatch.split('|');
            return values[indx] || '';
        });

        return strPluralize;
    },


    /**
     * Substitute and pluralize an string
     */
    subplur: function(strText, intNum, objReplace)
    {
        strText = this.pluralize(strText, intNum);
        strText = this.substitute(strText, objReplace);
        return strText;
    }
});


/**
 * Use with jQuery
 */
$.substitute = function(str, obj) {
    var sub = new dnSubPlur();
    return sub.substitute(str, obj);
};
$.pluralize = function(str, num) {
    var plur = new dnSubPlur();
    return plur.pluralize(str, num);
};
$.subplur = function(str, num, obj) {
    var subplur = new dnSubPlur();
    return subplur.subplur(str, num, obj);
};