# subplur

Substitute & Pluralize for JavaScript and jQuery. This is an contao-component.


## Usage

```javascript
var subplur = new dnSubPlur();

// Substitute
var string = 'This is an [example] for substitute.';
subplur.substitute(string, {example: 'test'}); // or with jQuery
$.substitute(string, {example: 'test'});

// Pluralize
var string = 'There {are|is} [num] apple{s} on the tree.';
subplur.pluralize(string, 1); // There is 1 apple on the tree.
$.pluralize(string, 1);

subplur.pluralize(string, 3); // There are 3 apples on the tree.
$.pluralize(string, 3);

// Substitute & Pluralize
var string = 'There {are|is} [num] [apple]{s} on the tree.';
subplur.subplur(string, 1, {apple: 'banana'}); // There is 1 banana on the tree.
$.subplur(string, 1, {apple: 'banana'});

subplur.subplur(string, 3, {apple: 'banana'}); // There are 3 bananas on the tree.
$.subplur(string, 3, {apple: 'banana'});
```